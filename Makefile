pdf:
	pdflatex main || exit 1
	bibtex main || exit 1
	makeglossaries main || exit 1
	pdflatex main || exit 1
	@echo Success.