\section{Introduction}

% todo inizia con in this thesis ... (oggetto)
% The subject of this master thesis consists in the tasks \textit{\acrfull{nel}}
% and \textit{NIL prediction} in the context of \textit{\acrfull{kbp}}.
\subsection{Background and Motivation}
% \subsubsection*{Knowledge Base Population}
% todo say something about incrementally ??

\textit{\acrfull{kbp}} is the task of automatically populating a
\textit{\acrfull{kb}} \cite{ji2011knowledge}, which in turn is a
computer-processable collection of knowledge about the world
\cite{rebele2016yago}. A \acrshort{kb} contains entities, that are uniquely
identifiable real-world objects, characterized by entity properties such as
name, type, attributes, and relationships with other entities. For instance, an
entity could represent a city named ``Rome'' with attributes like latitude,
longitude or population and with relationships such as \textit{is-capital-of}
``Italy'' \cite{DBLP:series/irs/Balog18}.

This work is part of the collaborative project \textit{Datalake@Giustizia}, to
which we simply refer as \textit{Giustizia}, that comes from the need of Italian
prosecutors for extracting information about suspects and relationships between
them from several folders and documents. These documents appear in various
shapes and sizes, such as unstructured text, structured or tabular data, and
even multimedia contents like videos or audio recordings. They are generated as
the investigation advances, and therefore we talk about incremental
\acrlong{kbp} because the population happens incrementally as new documents are
collected.

\textit{Giustizia} limits its focus to the analysis of unstructured text and
structured textual data and it aims at integrating both to create an
investigation-specific \acrlong{kb} using the techniques involved in
\acrlong{kbp}, eventually combined with human validation or correction
(\textit{\acrlong{hitl}}).
%
This way, prosecutors would be able to exploit the information in the
\Acrshort{kb} with minimal human effort, for instance, to quickly find
suspect-to-suspect relationships or the location of a suspect at a given time.


This work's focus is limited to \Acrshort{kbp} starting from natural language in
the form of free unstructured text. This implies several tasks of
\textit{\acrfull{nlp}} for \textit{\acrfull{ie}}. The most relevant ones for
this work are the following:
\begin{enumerate}
    \item \textit{\acrfull{ner}}, which identifies text spans that mention named
          entities and classifies them into predefined types such as person,
          location, and organization \cite{li2020nersurvey}.
          % todo che cos'è una named entity?? non l'ho spiegato
    \item \textit{\acrfull{nel}}, also called Entity Disambiguation, which aims
          at linking each mention with the corresponding entity in a
          \acrlong{kb} \cite{shen2015el}.
    \item \textit{NIL Prediction}, which is part of \Acrshort{nel} and consists
          in detecting when a mention refers to an entity that is not in the
          \acrshort{kb}; in this case, the mention is said to be \textit{NIL} or
          \textit{unlinkable} \cite{shen2015el}.
          %     From another point of view, let $e$ be the
          %     top-ranked entity for the linking of a mention $m$, the task answers the
          %     question ``Does the mention $m$ refer to $e$ or not?'', and if not $m$
          %     is NIL.
          % todo è corretto o va bene solo per il caso binario?
    \item \textit{Novel Entity Clustering}: in a \acrshort{kbp} context, the NIL
          mentions are usually clustered together with the ones that refer to the same
          missing entity, and finally the novel entities represented by each cluster
          are added to the \acrshort{kb}. % todo citare qualcosa?
\end{enumerate}
Figure \ref{fig:nernelnil} shows the first three tasks working on an example
sentence.


In this thesis, we did not address the tasks \acrshort{ner} and novel entities
clustering, which have been respectively treated collaboratively and by a
colleague.

Incremental \acrlong{kbp} implies an important challenge for \acrshort{nel}: all
the entities that will be added are obviously not available at training time.
Several \acrshort{nel} systems, however, obtained high performance by leveraging
prior information about the \acrshort{kb} and its entities at training time
\cite{logeswaran-etal-2019-zero}. These approaches are not suitable for
\acrshort{kbp} and the fact that in \textit{Giustizia} the \acrshort{kb}s are
investigation-specific reiterates that systems depending on a specific
\acrshort{kb} cannot be considered.

% todo uncomment the figure
\begin{figure}[h!]
    \begin{tabularx}{\textwidth}{cX}
        \toprule
        \texttt{(a)} & \texttt{Even Victoria, queen of the United Kingdom, loved
        cats, especially one named White Heather.} \\
        \hline
        \texttt{(b)} & \texttt{Even \textbf{Victoria (PER)}, queen of the
        \textbf{United Kingdom (LOC)}, loved cats, especially
        one named \textbf{White Heather (MISC)}.} \\
        \hline
        \texttt{(c)} & \texttt{Even \textbf{Victoria (dbr:Queen\_Victoria)},
        queen of the \textbf{United Kingdom (dbr:United\_Kingdom)},
        loved cats, especially one named \textbf{White Heather (NIL)}.} \\
        \bottomrule
    \end{tabularx}

    \hspace{\textwidth}

    \resizebox{\textwidth}{!}{%
    \begin{minipage}[t]{0.15\textwidth}
        \footnotesize{(a) The input sentence}
    \end{minipage}
    \hspace{0.025\textwidth}%
    \begin{minipage}[t]{0.2\textwidth}
        \footnotesize{(b) \acrlong{ner}}
    \end{minipage}
    \hspace{0.025\textwidth}%
    \begin{minipage}[t]{0.7\textwidth}
        \footnotesize{(c) \acrlong{nel} with NIL prediction linking to
        \textit{DBpedia}\footnote{\url{https://www.dbpedia.org/}} \acrshort{kb}.
        \textit{White Heather} is not present in DBpedia, thus is linked to
        NIL.} \end{minipage}}



    \caption{\acrshort{ner} and \acrshort{nel} with NIL prediction on an example sentence.}
    \label{fig:nernelnil}
\end{figure}

% \subsubsection*{Project Datalake@Giustizia}



\subsection{Objective} \label{sec:objective}
The objective of this master thesis is to explore the current advancements in
\acrshort{nel} and NIL prediction and adapt them to the requirements of
\acrlong{kbp} and \textit{Giustizia}, designing a suitable prototype for
\acrlong{nel} with NIL prediction.

Therefore, the main research question we aim to answer is:
\begin{enumerate}[leftmargin=1.5cm,label=\textit{RQ:},ref=\textit{RQ}]
    \item \textit{How state-of-the-art \acrshort{nel} systems can be adapted for
              a \acrshort{kbp} domain?}
\end{enumerate}
This question can be subdivided in three main sub-questions:
\begin{enumerate}[leftmargin=1.5cm,label=\textit{RQ\arabic*:},ref=\textit{RQ\arabic*}]
    \item \label{rq-represent-new-entities} \textit{How to represent
    newly-identified entities so that they are available for subsequent
    retrieval?}
    \item \label{rq-nil-detection} \textit{How state-of-the-art \acrshort{nel}
              systems can be adapted to identify NIL mentions?}
    \item \label{rq-kb-independent} \textit{How can we keep the system
              independent from the \acrshort{kb}?}
\end{enumerate}
Additionally, since \textit{Giustizia} considers the presence of human
validators, we treat the following question:
\begin{enumerate}[leftmargin=1.5cm,label=\textit{RQ4:},ref=\textit{RQ4}]
    \item \label{rq-hitl} \textit{How to best integrate the system with a \acrlong{hitl} process?}
\end{enumerate}

Note that the adaptation to the Italian language is not covered in this work
that considers instead the English language.

\subsection{Approach and Contribution}

We started with an analysis of the literature about \acrshort{nel} observing
that recently deep learning-based approaches achieved state-of-the-art results
\cite{wu2019scalable,de2020autoregressive}. We then restricted our research to
those systems supposed to answer the question \ref{rq-kb-independent}, which
turn out to be the ones capable of \textit{Zero-shot Entity Linking}
\cite{logeswaran-etal-2019-zero}. Among them, we proceeded by deeply analyzing
two
promising approaches: BLINK from \citet{wu2019scalable} and GENRE from
\citet{de2020autoregressive}.
%
After having compared the two systems with \acrshort{kbp} and
\textit{Giustizia}'s requirements in mind, even if GENRE surely introduced an
innovative approach, we opted for BLINK as it is conceptually simpler and we
propose the use of its dense representation of the mentions to represent the
novel entities for answering question \ref{rq-represent-new-entities}.

The NIL prediction task, instead, is often not considered by state-of-the-art
\acrshort{nel} systems, which used to exclude NIL mentions from the evaluation,
eventually leaving this task to future works \cite{wu2019scalable}. However,
several publications from the TAC (Text Analysis
Conference)\footnote{https://tac.nist.gov/} included studies on NIL prediction,
thus we reviewed the various techniques they adopted for the task.
%
We decided to design the NIL prediction module as a separated classifier
downstream to BLINK, using the linking scores as features for the classifier.
The classifier has been designed also to estimate the uncertainty in its
predictions so that it allows to limit the human intervention to uncertain
decisions, answering the research question \ref{rq-hitl}. Indeed, we opted for a
logistic regression model since it is suitable for modeling a probability
\cite{demaris1995tutorial}.

Some experiments were conducted to answer the discussed research questions. The
dataset employed in the experiments is mainly the AIDA CoNLL-YAGO
\cite{hoffart-etal-2011-robust}, as it provides NIL-annotated mentions (20\% of
the dataset). Additional datasets have been employed for testing only: MSNBC
\cite{cucerzan-2007-large}, AQUAINT \cite{milne2008learning}, ACE2004
\cite{ratinov-etal-2011-local}, WNED-WIKI (WIKI) and WNED-CWEB (CWEB)
\cite{guo2018robust}.

% % \subsubsection*{Linking to Mentions}
% To answer \ref{rq-represent-new-entities} we automatically constructed a
% \acrshort{kb} from scratch starting from the BLINK dense representation of the
% mentions from the AIDA training set. This procedure is referred as
% \textit{Linking to Mentions}, while \textit{Linking to Entities} refers to using
% the \acrshort{kb} created from the titles and the descriptions of the entities.
% Then the mentions from the AIDA test sets, excluding NIL ones, have been linked
% to the \acrshort{kb} using BLINK. The evaluation  demonstrating we can answer
% \ref{rq-represent-new-entities} using BLINK dense representation of the
% mentions. Furthermore, we noticed the \acrshort{kb} size impacts linking
% performance.

% % test on all datasets? talk about it here?

% % dire di più sul task di NIL prediction, probabilità 0-1

% % parlare bene del task obiettivo del NIL prediction

% % \subsubsection*{NIL prediction module features}
% For addressing question \ref{rq-nil-detection}, after having performed
% \acrshort{nel} with the approach \textit{Linking to Entities}, we run NIL
% prediction on the pairs $\langle$mention, top-ranked-entity$\rangle$ analyzing
% the following features in an ablation study: the linking scores of the best
% candidate, some statistics (mean, median, standard deviation) of the scores of
% the top-k candidates \cite{zhou2013description}, the Levenshtein and the Jaccard
% text similarities between the mention and the best candidate's title, and the
% types of the mention and the best candidate entity.
% %
% The models have been trained on the AIDA train set with undersampling to balance
% NIL and not-NIL classes and tested on the AIDA test sets with different
% combinations of features.
% %
% The best results are obtained combining multiple features; along with
% the top scores, the types and the statistics appear to give an important
% contribution while the contribution of the text similarities is moderate.
% % Quantitatively, we reach a macro average F1 score of 78.0 for NIL prediction and
% % an overall accuracy for \acrshort{nel} with NIL prediction of 69.4.

% % \subsubsection*{\acrlong{hitl} integration}
% The NIL prediction module proved to be able to correctly estimate the
% uncertainty of its predictions: most of the errors, indeed, are deemed as
% uncertain. Therefore, to answer question \ref{rq-hitl}, we simulated the
% integration of partial human validation and evaluated the system performance to
% the varying of the human intervention. By employing the model's confidence to
% decide which samples require human validation, rather than deciding randomly, we
% reduced the human effort by 10\%, still maintaining the same overall accuracy.

% % \subsubsection*{NIL prediction linking to Mentions}
% Another ablation study has been performed for the NIL prediction module, but
% \textit{Linking to Mentions} using the automatically created \acrshort{kb} from
% the AIDA train set. This study proved the statistics of the scores do not
% generalize successfully to different \acrshort{kb}s. The combination of multiple
% features still obtains the highest performance, and the types proved to be
% essential for estimating the uncertainty. We deduced that the maximum scores,
% the types, and the text similarities are compatible with \ref{rq-kb-independent}
% and are therefore selected for the prototype, while the statistics of the top-k
% candidates are not. The obtained quantitative results even outperform the ones
% when \textit{Linking to Entities}, although the smaller \acrshort{kb} could
% inflate the difference.
% %
% % An accuracy of 83.5 is reached for \acrshort{nel} with NIL prediction and a
% % macro average F1 score of 84.4 for the sole NIL prediction task.

% % \subsubsection*{Qualitative Analysis}
% Finally, we analyzed the errors qualitatively, evidencing some of the most
% frequent ones, like confusing the country with the national sports clubs and
% vice versa or confusing people sharing the same surname. We noticed the NIL
% prediction module often detected linking errors mitigating them by linking to
% NIL. Most of the errors committed by the NIL prediction module consist in
% mentions erroneously identified as NIL, half of which are estimated as
% uncertain. The NIL mentions incorrectly classified as not-NIL are often caused
% by improper annotations in the dataset. The analysis, indeed, revealed some
% inconsistencies in the AIDA datasets, such as the city of Turin annotated
% correctly three times but once wrongly as NIL: in this latter case, the system
% would probably link to Turin since the entity is in the \acrshort{kb}.

% We considered ambiguous mentions, such as ``Washington'' that could refer to
% multiple different entities, observing that the system is often unsuccessful,
% even if the NIL prediction model tends to assign uncertain or NIL results. For
% such ambiguous mentions, human validation may be worthwhile.



The contribution of this master thesis work can be summarized as follows:
\begin{itemize}
    \item the BLINK system has been adapted to the \acrshort{kbp} context by
          employing its dense representation for the retrieval of novel entities
          and by adding a NIL prediction module;
    \item different features for NIL prediction have been explored;
    \item a study has been conducted on how to estimate the system uncertainty
          to best integrate a \acrlong{hitl} process.
\end{itemize}

% conclusioni ok?

% % todo uncomment add outline
\subsection{Outline}

In Section \ref{sec:soa} we review the current advancements in \acrlong{nel},
and we describe the two approaches considered to be the \acrshort{nel} system in
the prototype, BLINK and GENRE, finally choosing BLINK. Then we examine the
literature for the NIL prediction task analyzing the different techniques
studied by several publications and we describe the approach we propose for the
prototype.

% Lastly, the decisions takend from the
% literature chosen to be part of the prototype are communicated.

Next, in Section \ref{sec:approach}, after a brief recap of the decisions
mentioned in the previous section, we explain how we intend to adapt BLINK for
\acrshort{kbp} and \textit{Giustizia} and we describe the design of the NIL
prediction module, providing a formal definition of the task. Moreover, we
introduce the experiments performed to verify that the prototype can answer
successfully to the defined research questions.

Section \ref{sec:results} describes the utilized datasets and provides the
experimental details as well as the results of the experiments.
%
While Section \ref{sec:discussion} analyzes the results and presents the derived
insights.

Finally, Section \ref{sec:conclusion} concludes this thesis summarizing the
findings and the performed approach while Section \ref{sec:futureworks} lists
the future works we consider worthwhile to improve the project.

% todo!!! dire che il lavoro è sull'inglese e che l'allenamento per l'italiano
% non è trattato