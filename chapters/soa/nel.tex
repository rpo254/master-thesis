\subsection{Entity Linking}
\acrlong{nel} is often a challenging task since an entity could appear in
several surface forms such as aliases and even the same mention could denote
different entities \cite{shen2015el}.

Generally, \acrlong{nel} comes in a pipeline after \acrlong{ner} that detects
the mentions to link, but recently some works proposed to jointly perform these
tasks to exploit their mutual dependency and avoid error propagation. This joint
task takes the name of \textit{end-to-end Entity Linking}
\cite{kolitsas-etal-2018-end}.

A \acrshort{nel} system usually features the following three steps:
\begin{itemize}
    \item \textit{Candidate Entity Generation}: for each mention, it filters a
          set of possibly relevant candidate entities from the \acrlong{kb}.
    \item \textit{Candidate Entity Ranking}: for each mention, the system ranks
          each of the candidate entities according to how likely that mention refers
          to that candidate.
    \item \textit{NIL Prediction or Unlinkable Mention Prediction}: it detects
          whether the mention refers to an entity existing in the \acrshort{kb} or
          to NIL.
          In practice, since the candidate entities have already been ranked in
          the previous step, it estimates whether the top-ranked entity is the
          correct one for the mention and if not it answers NIL.
\end{itemize}

In recent years deep learning-based approach obtained state-of-the-art results
by modeling the similarity between the mention context and the entity
description. \citet{he-etal-2013-learning}, in \citeyear{he-etal-2013-learning},
used auto-encoders to leverage contextual information,
\citet{sun-et-al-2015-modeling}, in \citeyear{sun-et-al-2015-modeling},
represented mentions contexts and entities in a vector space and then employed
the cosine similarity between mention and entity vectors for the ranking. Later
in \citeyear{yamada-etal-2016-joint}, \citet{yamada-etal-2016-joint} addressed
the problem of representing both mentions and entities in the same vector space
using the anchors from the \acrshort{kb} (Wikipedia) and their context for the
entities representation.
% luke sembra un evoluzione con obiettivo simile
\citet{ganea-hofmann-2017-deep}, in \citeyear{ganea-hofmann-2017-deep},
constructed the entity embeddings additionally considering the descriptions from
the \acrshort{kb} (Wikipedia) using an attention layer to select informative
words. Furthermore, they jointly resolve the mentions assuming coherence among
the entities in the same document. \citet{kolitsas-etal-2018-end}, in
\citeyear{kolitsas-etal-2018-end}, introduced the task of \textit{end-to-end
    Entity Linking} by identifying mentions and disambiguating them using the same
neural model instead of separating the two tasks.

\citet{raiman2018deeptype}, in \citeyear{raiman2018deeptype}, outperformed the
previous state-of-the-art by integrating a type system, and
\citet{onoe2020finegrained}, one year later, confirmed the important
contribution fine-grained entity types could bring to \acrshort{nel}.

\citet{logeswaran-etal-2019-zero}, in \citeyear{logeswaran-etal-2019-zero},
utilized a transformer-based approach to compute \textit{``cross-attention''}
between mentions with context and entity descriptions so that the same model
could exploit deep relations between the mention and the entity; this is
essentially what will be defined later as the \textit{cross-encoder}.
They also introduced the \textit{Zero-shot Entity Linking}, defining it as the
task of linking a mention to an unseen entity without any in-domain labeled
data. This task is particularly relevant for our context, since, to the best of
our knowledge, there's no dataset for \Acrshort{nel} in the legal domain for the
Italian language, thus the prospect of training with datasets from different
domains is promising for \textit{Giustizia}.

Furthermore, since in a \acrshort{kbp} context newly-identified entities are
unseen at training-time, those systems able to perform \textit{Zero-shot Entity
    Linking} can answer the research question \ref{rq-kb-independent} (\textit{``How
    can we keep the system independent from the KB?''}), while all the systems
limited only to the entities seen at training-time
are not suitable for \acrshort{kbp}.

\citet{gillick2019learning}, in \citeyear{gillick2019learning}, demonstrated the
dense representation can also improve the retrieval phase: they encoded mentions
and entities separately in the same vector space, similarly to what will be
later defined as the \textit{bi-encoder}, and performed the retrieval using an
approximate nearest neighbor search.

Subsequently in \citeyear{wu2019scalable}, \citet{wu2019scalable}, inspired by
the previous works and by the studies of \citet{humeau2020polyencoders}
(\citeyear{humeau2020polyencoders}) on \textit{bi-encoders} and
\textit{cross-encoders}, leveraged two fine-tuned BERT \cite{devlin2019bert}
transformers: the bi-encoder to obtain dense representations for candidate
retrieval and the cross-encoder for candidate reranking computing
cross-attention between mentions and entity descriptions.

\citet{Mulang__2020}, in \citeyear{Mulang__2020}, instead of using entities
descriptions, exploited the triples from the \acrshort{kb} (Wikidata) to give
the transformer contextual information about the entity: the triples have been
converted in natural language form and then fed into the transformer along with
the mention and its context.

At the end of \citeyear{de2020autoregressive}, \citet{de2020autoregressive}
proposed GENRE, the first system leveraging a generative approach to the
retrieval, effectively generating entity names starting from the mentions. Their
approach was able to mitigate some important shortcomings of the previous ones.

In this work, we analyzed deeply the two promising systems \textit{BLINK} by
\citet{wu2019scalable} and \textit{GENRE} by \citet{de2020autoregressive}, both
of which are capable of \textit{Zero-shot Entity Linking}.

\subsubsection*{Background}
Since both BLINK and GENRE exploit transformer-based architectures, we introduce
\textit{Transformers} and \textit{BERT} as well as some important concepts for
text representation.

When working on natural language, the text has to be represented in a
machine-readable way before any processing, and the quality of any following
task strongly depends on the quality of the representation
\cite{babic2020survey}.

The earliest approaches for encoding text into a vector, like \textit{Bag of
    Words (BOW)}, come with several shortcomings: for instance, vectors are usually
sparse \cite{zhixiang2013alternative}.

% \subsubsection{Word Embeddings}
\textit{Word Embeddings} overcome this sparsity limitation because they are by
definition dense fixed-length vectors representations for words. Nevertheless,
they also proved able to encode syntactic and semantic information
\cite{almeida2019word}.
Worth a mention are \textit{Continuous Bag of Words (CBOW)}, \textit{Skip-Gram
    (SG)}, which together are often referred as \textit{Word2Vec}
\cite{mikolov2013efficient}, and \textit{GloVe (Global Vectors)}
\cite{pennington2014glove}; they are all methods to calculate word embedding
vectors; the former two employ neural networks while the latter makes use of
global co-occurrence statistics.

With the publication of \citet{DBLP:journals/corr/VaswaniSPUJGKP17}, in
\citeyear{DBLP:journals/corr/VaswaniSPUJGKP17}, \textit{Transformers} started to
be widely employed in the field of \acrlong{nlp}. The architecture of a
Transformer is based on an \textit{encoder-decoder} neural model, which employs
an attention mechanism, effectively a self-attention, to grasp dependencies in
the input or output sequences \cite{DBLP:journals/corr/VaswaniSPUJGKP17}.

Nevertheless, these models are able to exploit the context of each word better
than previous models, like \textit{Recursive Neural Network (RNN)}
\cite{devlin2019bert}.

\textit{BERT (Bidirectional Encoder Representations from Transformers)} is a
widely-used transformer: it is designed to jointly exploit both the left and the
right context of the words (bidirectional) \cite{devlin2019bert}. The success of
BERT also derives from its language representation: it is pre-trained to learn a
generic language representation to then be fine-tuned for several downstream
tasks, including \acrshort{ner} and \textit{question answering}. The
fine-tuning, indeed, requires much less computation than a complete re-training.
In addition, BERT is effective for obtaining context-aware dense embeddings of
the input text, normally corresponding to the activations of the last neural
layer. For those tasks requiring a representation of the entire input sequence,
BERT provides a ``special classification token'' named $\mathtt{[CLS]}$, whose
final hidden state corresponds to the aggregate sequence representation.
% word-piece?
BERT input sequence is in the form of WordPiece embeddings, which instead of
treating words as atomic tokens consider them as composed by common sub-words
units, balancing the character-based models' flexibility in handling rare words
and the efficiency of word-based models
\cite{DBLP:journals/corr/WuSCLNMKCGMKSJL16}.

\subsubsection*{BLINK}
BLINK has been introduced with the work \textit{Scalable zero-shot entity
    linking with dense entity retrieval} (\citet{wu2019scalable}) and it is
available under the
\textit{MIT}\footnote{\url{https://github.com/facebookresearch/BLINK}} open-source
license.
The title of the publication presents most of its core features:
\begin{itemize}
    \item \textit{scalable}: it can retrieve $\mathtt{\sim}5.9$ million mentions
          in 2 milliseconds (using approximate retrieval).
    \item \textit{zero-shot}: it is capable of \textit{Zero-shot Entity
              Linking}.
    \item \textit{dense entity retrieval}: it retrieves the candidate entities
          after having represented them in a dense space. More precisely, each
          mention is encoded as a vector, as well as all the entities. Then the
          entities whose vectors are the most similar to the mention's one
          represent the best candidates for linking. To represent an entity, the
          title and a brief textual description are required.
\end{itemize}

In practice, BLINK consists of a two stages approach. The first stage handles
the retrieval in a dense space defined by the bi-encoder that independently
embeds mentions and entities, exploiting respectively the contexts and the
descriptions. In the second stage, each retrieved candidate is examined more
carefully by the cross-encoder that has at hand both the mention with its
context and the entity's title and description. Figure \ref{fig:blinkencoders}
provides a high-level description of the BLINK architecture.

\begin{figure}[h!]
    \centering
    \includegraphics[
        % height=5cm,
        width=\textwidth, keepaspectratio]{blink_encoders.png}

    \captionsource{High-level description of the BLINK architecture. From the
        top-left, the input mention gets encoded in the same dense space where
        all entities representations lie. A nearest neighbors search is then
        performed (depicted with a blue circle), and the k entities retrieved
        are supplied to the cross encoder. The latter reranks the candidates
        exploiting both input mention and entities descriptions producing a
        probability distribution.}{\citet{wu2019scalable}}
    \label{fig:blinkencoders}
\end{figure}

Both bi-encoder and cross-encoder consist in fine-tuned BERT models. The former
performs self-attention over mentions and candidate entities separately, tending
to be less accurate than the latter, which instead performs self-attention on
the concatenation of both the mention with its context and the entity's title
and description. However, the bi-encoder can compute the entities
representations in advance hence allowing fast retrieval at inference time
\cite{DBLP:journals/corr/abs-1905-01969}.

Let $y_m$ and $y_e$ be respectively the vector representations of the mention
and the entity; they correspond to the final state of the $\mathtt{[CLS]}$
special classification token.

Regarding the input of the bi-encoder, when encoding a mention it would be the
following:

\begin{equation}
    \mathtt{[CLS]}\;\;\;\;\mathtt{ctxt_l}\;\;\;\;\mathtt{[M_s]}\;\;\;\;\mathtt{mention}\;\;\;\;\mathtt{[M_e]}\;\;\;\;\mathtt{ctxt_r}\;\;\;\;\mathtt{[SEP]}
\end{equation}

\noindent where mention, $ctxt_l$, $ctxt_r$ are the WordPiece tokens of the
mention, context before and after the mention respectively, and $[M_s]$, $[M_e]$
are special tokens to tag the mention. Instead for encoding an entity the model
receives the following input:

\begin{equation}
    \mathtt{[CLS]}\;\;\;\;\mathtt{title}\;\;\;\;\mathtt{[ENT]}\;\;\;\;\mathtt{description}\;\;\;\;\mathtt{[SEP]}
\end{equation}

\noindent where title and description are the WordPiece tokens of entity title
and description, and $\mathtt{[ENT]}$ is a special separator token.

The score of the entity candidate $e$ for the mention $m$ is defined as the
dot-product of the two vectors $y_m$ and $y_e$:
\begin{equation}
    s(m, e) = y_m \cdot y_e
\end{equation}
%
The training of the network consists of maximizing the score of the correct
entity with respect to the entities of the same batch \cite{wu2019scalable}.

Once trained, the retrieval of candidate entities can be done with
\textit{Nearest Neighbor search} starting from the mention vector
representation. BLINK handles indexing and retrieval through the use of
\textit{FAISS} \cite{johnson2017billionscale}, which provides approximate
nearest neighbor algorithms \cite{malkov2018efficient} as well as exact ones.


The cross-encoder, instead, receives as input both the mention and the entity:
essentially its input consists in the concatenation of the two different inputs
accepted by the bi-encoder divided by a separator token, as follows:

\begin{equation}
    \mathtt{[CLS]}\;\mathtt{ctxt_l}\;\mathtt{[M_s]}\;\mathtt{mention}\;\mathtt{[M_e]}\;\mathtt{ctxt_r}\;\mathtt{[SEP]}\;\mathtt{title}\;\mathtt{[ENT]}\;\mathtt{description}\;\mathtt{[SEP]}
\end{equation}
%
% This allows the model to have deep cross-attention between the context and
% entity descriptions.
Then, let $y_{m,e}$ be the final state of the $\mathtt{[CLS]}$ token
corresponding to the input representation, a linear layer $W$ is applied
downstream to $y_{m,e}$ to obtain a score for the candidates:

\begin{equation}
    s_{cross}(m,e) = y_{m,e}W
\end{equation}

The model is trained to maximize $s_{cross}$ for the correct entity given a
limited ($\leq 100$) candidate set. In fact, this architecture requires large
amounts of memory and computation, resulting not suitable for the retrieval.

% \paragraph{Available models and \acrlong{kb}}

\citet{wu2019scalable} made publicly available some BLINK models pre-trained on
the May 2019 English Wikipedia dump, which includes 5.9M entities, using the
hyperlinks in the articles as training samples where the anchor text is a
mention of the entity to which the hyperlink refers. The available indexes are
created after the representations of the entities from the same Wikipedia dump.
During this work the \acrshort{kb} corresponding to the Wikipedia dump and on
which the indexes rely will be referred as the \textit{BLINK \acrlong{kb}}.

\subsubsection*{GENRE}
GENRE (Generative ENtity
REtrieval)\footnote{\url{https://github.com/facebookresearch/GENRE}} was born from
the work \textit{Autoregressive entity retrieval} by
\citet{de2020autoregressive} in \citeyear{de2020autoregressive} and it is
available under the \textit{Creative Commons} open-source license. It is
presented as \textit{``the first entity retriever that exploits a
    sequence-to-sequence architecture to generate entity names in an autoregressive
    fashion conditioned on the context''}. It is based on a pre-trained transformer
named BART \cite{lewis2019bart}, which proved to be particularly effective for
text generation.
%
BART has then been fine-tuned to generate entity identifiers. When linking a
mention, GENRE computes a score for each entity in an autoregressive way,
therefore exploiting previous linkings. The training consists in maximizing the
score of the correct mention-entity pairs or, more precisely, the logarithm of
the score.

GENRE does not treat entity names atomically but instead as a concatenation of
multiple atomic tokens, since entity names, like Wikipedia identifiers, are
often unambiguous, highly structured, and compositional. Furthermore, they
frequently contain information useful to disambiguate that could interact
with mention context.

Since, at inference time, it wouldn't be feasible to compute the score of every
possible token combination, GENRE relies on Beam Search
\cite{sutskever2014sequence}, an established approximate decoding strategy to
efficiently navigate the search space. Indeed by searching for the
top-\textit{k} entities using \textit{k} beans, the time cost of the retrieval
only depends on the size of the beams and on the average length of the entity
representations (Wikipedia titles are composed by six tokens on average).

Anyway, Beam Search might lead to the generation of a not-existing entity
identifier (not all token combinations corresponds to an entity), so a
Constrained Beam Search is employed along with a prefix tree where nodes are
annotated with tokens from the vocabulary and each leaf corresponds to an entity
identifier. This way, the next candidate token is conditioned by the previous
ones.

GENRE has been evaluated in multiple tasks: \acrlong{nel}, \textit{end-to-end
    Entity Linking} and document retrieval. The figure \ref{fig:genre} shows GENRE
performing \textit{end-to-end Entity Linking} starting from an example sentence.
To understand how GENRE works for \acrshort{nel} it is sufficient to look at
step \textit{c}, supposing the mention has already been identified by a separate
\acrshort{ner} system.

\begin{figure}[h!]
    \centering
    \includegraphics[
        % height=5cm,
        width=\textwidth, keepaspectratio]{genre.png}

    \begin{minipage}[t]{0.26\textwidth}
        \footnotesize{(a) Outside: we can either continue to generate the input or
            start a new mention.}%
    \end{minipage}%
    \hspace{0.025\textwidth}%
    \begin{minipage}[t]{0.3\textwidth}
        \footnotesize{(b) Inside a mention: we can either continue to generate the
            input or end the current mention.}%
    \end{minipage}%
    \hspace{0.025\textwidth}%
    \begin{minipage}[t]{0.39\textwidth}
        \footnotesize{(c) Inside an entity link: we can either generate from the
            entities prefix trie or close if the generated prefix is a valid
            entity.}%
    \end{minipage}%
    \caption{%
        Example of GENRE performing end-to-end entity linking on the sentence
        \textit{``In 1503, Leonardo began painting the Mona Lisa.''}. There
        are 3 cases: when we are outside a mention/entity (a), inside a
        mention generation step (b), and inside an entity link generation
        step (c). In case the mentions are identified from a separate
        \acrshort{ner} system, GENRE would skip steps (a) and (b) starting
        from the given mentions with steps (c). The model is supposed to
        identify mentions and annotate with the correct entities: ``In 1503,
        [Leonardo](Leonardo da Vinci) began painting the [Mona Lisa](Mona
        Lisa)''.%
        \\\hspace{\linewidth}%
        \textbf{Source:} \citet{de2020autoregressive}%
    }%
    \label{fig:genre}
\end{figure}

\FloatBarrier In comparison to GENRE, the previous systems based on a classifier
approach, like BLINK, come with several shortcomings:
\begin{itemize}
    \item unless a costly cross-encoder is used for reranking, the dot-product
          can miss fine-grained interactions between the mention and the entity;
    \item storing dense vectors for the whole KB requires a large memory
          footprint;
    \item at training time, for computational reasons, it is required to
          sub-sample negative data;
    \item they require more information to represent entities, for instance
          BLINK requires a description and a title, while for GENRE a meaningful
          title is sufficient.
\end{itemize}

Table \ref{tab:genreblink} compares GENRE and BLINK performances on several
datasets for both in-domain and out-of-domain settings: GENRE outperforms BLINK
on all datasets.

\begin{table}[h]
    \resizebox{\textwidth}{!}{%
        \begin{tabular}{ lc|ccccc|c }
            \toprule
                                              & \textbf{In-domain} & \multicolumn{5}{c|}{\textbf{Out-of-domain}} &                                        \\
                                              & AIDA               & MSNBC                                       & AQUAINT & ACE2004 & CWEB & WIKI & Avg.
            \\
            \midrule
            BLINK                             & 79.6               & 80.0                                        & 80.3    & 82.5    & 64.2 & 75.5 & 77.0 \\
            \footnotesize{w/o candidate set*} &                    &                                             &         &         &      &      &      \\
            \hline
            GENRE                             & 93.3               & 94.3                                        & 89.9    & 90.1    & 77.3 & 87.4 & 88.8 \\
            \hline
            GENRE                             & 91.2               & 86.9                                        & 87.2    & 87.5    & 71.1 & 86.4 & 85.1 \\
            \footnotesize{w/o canted set*}    &                    &                                             &         &         &      &      &      \\
            \bottomrule
        \end{tabular}}

    \captionsource{Comparison of GENRE and BLINK for \acrshort{nel} reporting
        micro F1 (InKB) on one in-domain test set: AIDA CoNLL-YAGO
        \cite{hoffart-etal-2011-robust}, and five out-of-domain test sets: MSNBC
        \cite{cucerzan-2007-large}, AQUAINT \cite{milne2008learning}, ACE2004
        \cite{ratinov-etal-2011-local}, WNED-WIKI (WIKI) and WNED-CWEB (CWEB)
        \cite{guo2018robust}. *using the entire collection of entities as
        candidates, instead of a subset.}{\citet{de2020autoregressive}}
    \label{tab:genreblink}
\end{table}

% \paragraph{Available models}
\citet{de2020autoregressive} made available several pre-trained models,
including two for \textit{Entity Disambiguation}: one is pre-trained on BLINK
data only and the other on BLINK data and then fine-tuned on AIDA CoNLL-YAGO
\cite{hoffart-etal-2011-robust}. The available prefix tree refers to the
Wikipedia titles from
KILT\footnote{\url{https://github.com/facebookresearch/KILT}} derived from an
August 2019 Wikipedia dump.

\subsubsection*{BLINK and GENRE for \acrlong{kbp}}

Table \ref{tab:genreblink4giustizia} shows a comparison of GENRE and BLINK with
respect to the \acrshort{kbp} context and \textit{Giustizia} requirements.
Finally, we opted for BLINK to be the \acrshort{nel} system at the basis of the
prototype for the following reasons:
\begin{itemize}
    \item it is conceptually simpler than GENRE,
    \item the dense representation of the mentions is promising for representing
          newly-identified entities and retrieving them for future linkings
          (research question \ref{rq-represent-new-entities}),
    \item the dense representation may be suitable for clustering together NIL
          mentions that refer to the same entity.
\end{itemize}

\begin{table}[h]
    \resizebox{\textwidth}{!}{%
        \begin{tabular}{ lll }
            \toprule
                                      & \textbf{BLINK}               & \textbf{GENRE} \\
            \midrule
            NIL prediction            & No                           & No             \\
            \hline
            Manually add new entities & Needs Title and Description                   %
            % \tablefootnote{In
            %     \acrshort{kbp} there is no description provided for new entities but only
            % some mentions and context}%
                                      & Needs a meaningful title                      %
            % \tablefootnote{GENRE showed
            %     drastic performance drops using IDs instead of meaningful titles
            % \cite{de2020autoregressive}}%
            \\
            \hline
            Add novel entities        & Use the dense
            representation%
            % \tablefootnote{BLINK dense representation is not thought
            %     for clustering, but ideally mentions of the same entity would appear, in
            %     the dense space, near the referenced entity, therefore being close to
            % each other}%
                                      & Generate unconstrained                        \\
            from context              & of the mentions              & titles         %
            % \tablefootnote{GENRE is not trained
            % to provide a meaningful dense space in which mentions are encoded}%
            \\
            \hline
            Cluster mentions          & Use the dense representation & No             \\
            \hline
            Disk and Memory           & High                         & Low            \\
            requirements              &                              &                \\
            \hline
            Time requirements         & Cross-encoder is slow        & Fast           \\
            % & Exact Nearest Neighbor retrieval is slow & \\ % true?
            \bottomrule
        \end{tabular}}
    \caption{Comparison of GENRE and BLINK for \acrshort{kbp} and \textit{Datalake@Giustizia}}
    \label{tab:genreblink4giustizia}
\end{table}

However, the context implies some disadvantages: when a new entity is detected
no description is provided, but BLINK is trained to receive the entity
description as input.

% % lo dico già nel method
% An idea to overcome this limitation is to train the cross-encoder with a
% different objective task: given two mentions, estimate how likely they refer to
% the same entity. This way, previous mentions would make linking possible even
% without the entity description.

About GENRE for \acrshort{kbp} instead, the idea of generating novel entity
names appears certainly interesting, but it is not addressed in this work.
