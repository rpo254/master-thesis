\subsection{NIL Prediction}

Unfortunately, the NIL prediction task is often ignored by \acrshort{nel}
systems that tend to omit NIL mentions from the datasets when evaluating and
eventually leave this task to future works. Indeed, out of the 38 approaches
compared by the survey \citet{sevgili2021neural} only 8 included NIL prediction.

% dico che le TAC hanno dato importanza al NIL
However, the TAC\footnote{\url{https://tac.nist.gov/}} (Text Analysis
Conference) included NIL prediction in its \acrshort{kbp} track starting from
2009 \cite{mcnamee2009overview}, and since 2011 it is required to cluster
together those NIL mentions that refer to the same entity \cite{ji2011tac}. This
conference is organized by the NIST\footnote{\url{https://www.nist.gov/}} to
encourage research in \acrshort{nlp} and related applications by providing a
large test collection and common evaluation procedures. Since 2008 several TAC
editions took place, one per year, stimulating various research works that
treated the NIL prediction task, some of them giving it more focus than others.

% now summarize the approaches used by different papers



\acrlong{nel} with NIL prediction is essentially a classification with a reject
option that consists in NIL or unlinkable. There are four common approaches to
perform NIL prediction \cite{sevgili2021neural}:
\begin{enumerate}[leftmargin=1.5cm,label=\textit{NIL\textsubscript{\arabic*}:},ref=\textit{NIL\textsubscript{\arabic*}}]
    \item \label{nilm-empty-set} Linking to NIL when the candidate generation
          gives an empty set of candidates.
    \item \label{nilm-threshold} Setting a threshold for the linking score,
          below which a mention is considered unlinkable.
    \item \label{nilm-add-class} Adding a NIL entity to the candidates set, in
          the ranking phase. Hence a new class is added to a classification
          problem.
    \item \label{nilm-binary-clf} Training an additional binary classifier that
          accepts as input mention-entity pairs possibly with additional
          features, such as best linking score or information from
          \acrshort{ner}, and that finally classifies whether a mention is
          linkable or not.
\end{enumerate}
Some publications \cite{Zhang2012TheNE, Rao2013EntityLF} state method
\ref{nilm-threshold} (threshold) and \ref{nilm-add-class} (additional class) are
equivalent even though the latter requires no hand-tuning to set the threshold.

Generally, these approaches come inside a pipeline-based system that considers NIL
prediction and \acrshort{nel} as separate tasks. Some works, in opposition,
proposed a single joint system to avoid error propagation and to exploit
inter-task dependencies \cite{fahrni2013hits,martins-etal-2019-joint}.

Table \ref{tab:nilapproaches} shows the different approaches for NIL prediction
employed by some relevant papers, most of which come from TAC editions.


% now create a table to show the analyzed papers and their features and insights
% (\citeyear{sevgili2021neural})
\begin{table}[h!]
    \centering
    \resizebox{\textwidth}{!}{%
        \begin{tabular}{ clcll }
            \toprule
            \textbf{TAC} & \textbf{Publication}                 & \textbf{Year}                           & \textbf{Approach} & \textbf{Comments}    \\
            \midrule
                         & \citet{martins-etal-2019-joint}      & \citeyear{martins-etal-2019-joint}      & Joint Approach    & LSTM-based           \\
                         &                                      &                                         &                   & NER, NEL, NIL        \\
            \hline
                         & \citet{10.1007/978-3-319-58068-5_21} & \citeyear{10.1007/978-3-319-58068-5_21} & 4 (Binary Clf)    &                      \\
            \hline
                         & \citet{Broscheit_2019}               & \citeyear{Broscheit_2019}               & 3 (Add Class)     & BERT and             \\
                         &                                      &                                         &                   & classification layer \\
            \hline
                         & \citet{Rao2013EntityLF}              & \citeyear{Rao2013EntityLF}              & 3 (Add Class)     &                      \\
            \hline
            2017         & \citet{BernierColborne2017CRIMsSF}   & \citeyear{BernierColborne2017CRIMsSF}   & 2 (Threshold)     &                      \\
            \hline
            2017         & \citet{Li2017AHM}                    & \citeyear{Li2017AHM}                    & 3 (Add Class)     &                      \\
            \hline
            2017         & \citet{Jiang2017SRCBED}              & \citeyear{Jiang2017SRCBED}              & 2 (Threshold)     &                      \\
            \hline
            2017         & \citet{Yang2017TheTS}                & \citeyear{Yang2017TheTS}                & 4 (Binary Clf)    & Additional features: \\
                         &                                      &                                         &                   & Second's score       \\
                         &                                      &                                         &                   & Stdev of top-k       \\
            \hline
            2017         & \citet{klang2019overview}            & \citeyear{klang2019overview}            & 3 (Add Class)     & Reranker does NIL p.    \\
            \hline
            2016         & \citet{tan2015buptteam}              & \citeyear{tan2015buptteam}              & 2 (Threshold)     &                      \\
            \hline
            2016         & \citet{Paikens-EtAl:2017:TAC-KBP}    & \citeyear{Paikens-EtAl:2017:TAC-KBP}    & Other             & Uses coherence       \\
                         &                                      &                                         &                   & and co-reference     \\
                         &                                      &                                         &                   & between mentions     \\
            \hline
            2013         & \citet{yang2013buptteam}             & \citeyear{yang2013buptteam}             & 4 (Binary Clf)    &                      \\
            \hline
            2013         & \citet{chen2013casia}                & \citeyear{chen2013casia}                & 3 (Add Class)     &                      \\
            \hline
            2013         & \citet{fahrni2013hits}               & \citeyear{fahrni2013hits}               & Joint Approach    & NEL, NIL,            \\
                         &                                      &                                         &                   & NIL clustering       \\
            \hline
            2013         & \citet{Byrne2013UCDIA}               & \citeyear{Byrne2013UCDIA}               & 2 (Threshold)     &                      \\
            \hline
            2013         & \citet{huang2013msr}                 & \citeyear{huang2013msr}                 & 2 (Threshold)     &                      \\
            \hline
            2013         & \citet{Pink2013SYDNEYCA}             & \citeyear{Pink2013SYDNEYCA}             & 4 (Binary Clf)    &                      \\
            \hline
            2013         & \citet{barrena2013ubc}               & \citeyear{barrena2013ubc}               & 3 (Add Class)     &                      \\
            \hline
            2013         & \citet{dalton2013umass}              & \citeyear{dalton2013umass}              & 2 (Threshold)     &                      \\
            \hline
            2013         & \citet{zhou2013description}          & \citeyear{zhou2013description}          & 4 (Binary Clf)    & Statistical features \\
            \hline
            2012         & \citet{Fahrni2012HITSMA}             & \citeyear{Fahrni2012HITSMA}             & Joint Approach    & NEL, NIL,            \\
                         &                                      &                                         &                   & NIL clustering       \\
            \hline
            2012         & \citet{monahan2012lorify}            & \citeyear{monahan2012lorify}            & 4 (Binary Clf)    &                      \\
            \hline
            2012         & \citet{zhang2012nlprir}              & \citeyear{zhang2012nlprir}              & 3 (Add Class)     &                      \\
            \hline
            2011         & \citet{mcnamee2011cross}             & \citeyear{mcnamee2011cross}             & 3 (Add Class)     &                      \\
            \bottomrule
        \end{tabular}} \caption{Summary of the approaches for NIL prediction
        adopted by some relevant publications, most of which have been presented
        during a TAC edition according to the year specified in the table.}
    \label{tab:nilapproaches}
\end{table}

In this work we opt for the approach \ref{nilm-binary-clf}, the binary
classifier, as it allows to exploit additional features, such as statistics of
the top candidates \cite{Yang2017TheTS}, without modifying the linker
architecture.

As for the approach \ref{nilm-empty-set} (no candidates found), it is evident
that when there are no candidates for a mention, then the mention is NIL.
Anyway, this is unlikely to happen in systems like BLINK that are based on
nearest neighbor search: unless the \acrshort{kb} is empty these \acrshort{nel}
systems will always generate a not-empty set of candidates.
