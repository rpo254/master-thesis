\section{Conclusion} \label{sec:conclusion}

In this master thesis project, we explored the advancements in \acrlong{nel} and
NIL prediction to adapt them for \acrlong{kbp} and \textit{Giustizia} with the
final aim of designing a prototype for \acrshort{nel} with NIL prediction.

In section \ref{sec:objective} we defined four research questions that represent
the requirements of the context. We repeat them here for the sake of clarity:
\begin{enumerate}[leftmargin=1.5cm,label=\textit{RQ\arabic*:},ref=\textit{RQ\arabic*}]
    \item \textit{How to represent
    newly-identified entities so that they are available for subsequent
    retrieval?}
    \item \textit{How state-of-the-art \acrshort{nel}
              systems can be adapted to identify NIL mentions?}
    \item \textit{How can we keep the system
              independent from the \acrshort{kb}?}
    \item \textit{How to best integrate the system with a \acrlong{hitl} process?}
\end{enumerate}

Initially, we investigated the state-of-the-art approaches for \acrshort{nel}
and NIL prediction. For the former, we observed that recently the research
direction is towards deep learning-based approaches. Thus we analyzed deeply two
promising ones: BLINK by \citet{wu2019scalable} and GENRE by
\citet{de2020autoregressive}. Both of them can perform \textit{Zero-shot Entity
Linking} and hence are promising for answering question \ref{rq-kb-independent}.
Regarding NIL prediction, instead, we noticed that often state-of-the-art
\acrshort{nel} systems ignored the NIL prediction task. Then, we reviewed
several works derived from the TAC\footnote{https://tac.nist.gov/} that included
studies on the NIL prediction.

We opted for BLINK to be the \acrshort{nel} system of the prototype because it
is conceptually simpler than GENRE and because its dense representation of
mentions and entity is promising for question \ref{rq-represent-new-entities}.
%
For the NIL prediction, we designed a separate module based on a
\textit{Logistic Regression} model that is suitable for describing a probability
and therefore promising for answering question \ref{rq-hitl}. We included the
NIL prediction module in a pipeline-based system, downstream to BLINK. This
architecture is our proposal to answer the research question \ref{rq-nil-detection}.
Some experiments have been then performed to verify the designed prototype is
able to successfully answer the research questions.

The datasets considered for the experiments are the following: AIDA CoNLL-YAGO
\cite{hoffart-etal-2011-robust}, MSNBC \cite{cucerzan-2007-large}, AQUAINT
\cite{milne2008learning}, ACE2004 \cite{ratinov-etal-2011-local}, WNED-WIKI
(WIKI) and WNED-CWEB (CWEB) \cite{guo2018robust}. However, most of the
experiments employed just the AIDA dataset, since it is the only one that
contains NIL-annotated mentions.

The experiment described in \ref{sec:method-novel-entity-repr} proved that BLINK
is able to answer the research question \ref{rq-represent-new-entities} by using
its dense representation of the mentions. Indeed the linking accuracy obtained
during the evaluation reaches 92\% with the best configuration (ignoring NIL
mentions)%
%; see \ref{sec:linking-from-mentions}
. Interestingly the performance
obtained \textit{Linking to Mentions} even outperforms the default approach
\textit{Linking to Entities}, demonstrating the effectiveness of the BLINK
mentions representation for \acrshort{kbp}. Furthermore, we quantified the
impact of the \acrshort{kb} size, deducing that a lighter \acrshort{kb} is
beneficial for the linking accuracy. Finally, about the approaches for indexing
the clusters of mentions, the ``All'' criterion proved to be the most effective
one, suggesting that a single point is not enough to represent exhaustively a
cluster and hence studies on how best to represent the clusters may be
worthwhile.

To verify the designed prototype effectively answers the question
\ref{rq-nil-detection}, we performed \acrshort{nel} with NIL prediction
considering different features for the NIL prediction module in an ablation
study (\ref{sec:features-ablation-study})%
% training the models on the AIDA
% training set using random undersampling to balance NIL and not-NIL classes and
% testing on the AIDA test sets combined
.
% The following featurs
% have been considered: the scores of the best candidate for linking, some
% statistics of the scores of top-k candidates, the Levenshtein and the Jaccard
% text similarities, and the typing information of mentions and entities.
Among the considered features, the NER types and the statistics of the top-k
candidates scores appeared to be the most effective ones, even if the subsequent
experiments proved the latter are not independent of the \acrshort{kb}.
Furthermore, this experiment demonstrated that the prototype outperforms the
baseline, which corresponds to BLINK without NIL prediction, thus answering
question \ref{rq-nil-detection}. For the AIDA dataset, the macro average F1
scores for the NIL prediction (between NIL and not-NIL classes) is 78.0, while
the overall accuracy of the prototype for \acrshort{nel} with NIL prediction is
69.4, with the best configuration. The evaluation on all the datasets
(\ref{res:eval-all-data}) obtained generally worse results than the baseline
since most datasets lack NIL mentions, but, when considering as correct the
linking errors mitigated by the NIL prediction, the performance appears very
similar to the baseline. Note that the types were not considered since only AIDA
provide typing information.

We simulated the integration of a human validator (\acrlong{hitl}) to verify
that our decisions answer the question \ref{rq-hitl}. As seen in \ref{sec:hitl},
the logistic regression model proved able to model the system uncertainty.
Furthermore, the use of the model confidence to submit only uncertain samples to
the human validator allowed to reduce the required human effort by 10\% in the
simulation.

Next, an ablation study of the NIL prediction features has been conducted
similarly to the previous one but \textit{Linking to Mentions}, to evaluate the
entire system in a \acrshort{kbp} context (\ref{sec:linking-from-mentions-nil}).
This study revealed the statistics of the scores are not independent of the
\acrshort{kb} and that the types are crucial for the model to estimate the
uncertainty. Therefore to answer the question \ref{rq-kb-independent} we
consider as features for NIL prediction module the best candidate scores, the
types, and the Jaccard similarity between the mention surface form and the
entity title. The obtained results even outperform the ones of the study
conducted \textit{Linking to Entities} showing the effectiveness of the
prototype in a \acrshort{kbp} context, even if the performance gap could be
inflated by different sizes of the \acrshort{kb}s.

Finally, we analyzed qualitatively the results of the system, especially the
errors committed. Looking at the samples wrongly identified as NIL
% with the
% approach \textit{Linking to Mentions}
we recognized some common patterns such as surnames without the names or sports
clubs confused with countries. We believe these errors are due to BLINK scores
being relatively low. This reveals the importance of the \acrshort{nel} module,
whose uncertainty can be transmitted to the NIL prediction module. Moreover, we
noticed the AIDA datasets contain some inconsistencies regarding the
annotations, such as notorious entities annotated as NIL, including Pope John
Paul II or Turkey.
%
We analyzed the prototype behavior for ambiguous mentions like ``Washington'',
realizing the system often commits mistakes like confusing the city with the
state, while the NIL prediction module tends to give NIL answers or assign
uncertain scores. Therefore we believe human validation may be worthwhile for
ambiguous mentions.
%
Finally, we identified some interesting errors showing the possible shortcomings
of the approaches, like BLINK, based on the semantic representation of mentions
and entities. Indeed semantically similar entities can be confused: some
examples are the Vatican City and the Holy See, London and the United-Kingdom,
countries and national football teams, and even athletes with sports clubs.


% nil corrects nel. è più quantitativo? magari accennare quando analizzo
% linking errors

% when blink scores are low the nil prediction may fail
% hitl for ambiguos cases
% inconsistencies in the dataset

% dire cosa è emerso dall qualitative analysis


% todo! ner types va bene? cambiare in qulacos'altro? ner-wikidata


% recall what has been done and the objectives

% some conclusion out of the discussion: the dense representation has some
% downsides...

% how we answered research questions

% summary of the constributions

% remove this from the intro?

% todo più nello specifico
In view of the results, the contribution of this master thesis work can be
summarized as follows:
\begin{itemize}
    \item The BLINK system has been adapted to the \acrshort{kbp} context by
          employing its dense representation for the retrieval of novel entities
          and by adding a NIL prediction module that receives the following
          input features: BLINK scores of the best candidate for linking, the
          types of the mention and the best candidate entity, and the Jaccard
          text similarity between the mention surface form and the title of the
          best candidate entity.
    \item Different features for NIL prediction have been explored, finding that
          the types can give an important contribution to the task and that the
          statistics of the top-k scores depend on the \acrshort{kb}.
    \item It has been proven that a logistic regression model is effective in
          estimating the system uncertainty, which in turn can be exploited to
          integrate a \acrlong{hitl} process efficiently balancing the human
          effort.
\end{itemize}