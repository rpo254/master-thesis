\section{Approach} \label{sec:approach}


Recalling the decisions mentioned in the previous section \ref{sec:soa}, after
opting for BLINK to be the \acrshort{nel} system for the prototype, we designed
an additional NIL prediction module placed downstream to BLINK in a pipeline
system, thus without modifying the BLINK architecture. This configuration is
essentially our proposal for answering the question \ref{rq-nil-detection}
(\textit{How state-of-the-art \acrshort{nel} systems can be adapted to identify
NIL mentions?}).

Nevertheless, being aware that performing \acrshort{nel} and NIL prediction
jointly with a single component is promising
\cite{fahrni2013hits,martins-etal-2019-joint}, we leave it to future works.

In the following subsection we first present the proposed approach to represent
novel entities with BLINK (research question \ref{rq-represent-new-entities}),
in \ref{sec:method-novel-entity-repr}. Next, in \ref{sec:method-nil-prediction},
we describe the design of the NIL prediction module, providing a formal
definition of the task. Finally, we introduce the experiments performed to
verify that our decisions answer the defined research questions; the results
are available in section \ref{sec:results}.

\subsection{Novel Entities Representation} \label{sec:method-novel-entity-repr}

Considering the research question \ref{rq-represent-new-entities} (\textit{How
to represent newly-identified entities so that they are available for subsequent
retrieval?}) together with the architecture of BLINK, some complications arise.
Indeed, BLINK is built to represent the entities given a title and a short
textual description, but neither the title nor the description of the
newly-identified entities are available in a \acrshort{kbp} context: the
information about the new entities consists only in the mentions with their
surface forms and contexts. Furthermore, in \citet{wu2019scalable} the FAISS
index, on which BLINK relies for the retrieval, is created by adding all the
entities at once and never updated, while the context implies the incremental
addition of new entities.

Therefore the requirements for adapting BLINK to a \acrshort{kbp} context can be
summarized as follows:
\begin{enumerate}
    \item represent newly-identified entities using their mentions in their
          context;
    \item add new entities to an existing index.
\end{enumerate}

The second point strictly depends on the index type. BLINK comes with two
possible indexing configurations of the FAISS library: an approximate index for
faster retrieval (using the HNSW \cite{DBLP:journals/corr/MalkovY16} protocol)
and a ``Flat'' index for exact search. Unfortunately, the approximate one cannot
be extended incrementally: it supports the $L^2$ distance only while BLINK is
trained to maximize the dot-product and even if a conversion of the vector space
is possible it requires to index all the vectors at once\footnote{Look at the
file ``blink/indexer/faiss\_indexer.py'' at
\url{https://github.com/facebookresearch/BLINK} from line 86} resulting
inadequate for \acrshort{kbp}. The flat index, instead, natively supports
dot-product and it allows incremental population although it is slower than the
approximate one. This is acceptable for the \acrshort{kb} of an investigation
but could generate problems when using larger pre-existing knowledge bases: for
instance, the BLINK \acrshort{kb} based on Wikipedia contains 5.9 million
entities. As a solution, large pre-existing knowledge bases could be indexed in
advance using the approximate index, while the incremental population would use
the flat one. Both indexes can coexist since what really matters is the
dot-product between mentions and candidates representations, hence, regardless
of from which index the candidates are retrieved, computing the dot-product
would be sufficient for ranking them.


About the first point, instead, BLINK bi-encoder perfectly suits the
\acrshort{kbp} needs. Since it is trained to encode mentions and entities in the
same space, using the encodings of the mentions to represent the
newly-identified entities would allow to retrieve them for future linkings.
Considering a cluster of $n$ NIL mentions referring to an entity $e$, since each
of the $n$ mentions would have a different vector representation, we explored
three different approaches to represent $e$:
\begin{description}[labelwidth=4em,leftmargin=\dimexpr\labelwidth+\labelsep\relax,font=\normalfont\itshape\space]
    \item [First:] indexing the representation corresponding to the first mention encountered,
    \item [Medoid:] indexing the medoid of $n$ representations,
    \item [All:] indexing all the representations of the $n$ mentions: as a
          result multiple instances in the index would refer to the same entity
          $e$.
\end{description}
We refer to these approaches as ``indexing approaches''.

However, also the more accurate cross-encoder requires the title and the
description of the entity, which are not available. Actually, given a mention
with its context and a candidate entity with its title and description, the
cross-encoder estimates how likely that mention refers to that entity. This task
may be modified, instead, to estimate how likely two mentions refer to the same
entity. When identifying a novel entity, it would be sufficient to save a
representative mention and its context for future linkings. This task adaptation
would probably require a re-training of the cross-encoder; we decided to leave
it to future works relying on the bi-encoder only.


To verify our suppositions, we simulated the population of a \acrlong{kb}
starting from scratch and incrementally adding new entities starting from their
mentions using the representations from the bi-encoder considering all the three
indexing approaches explained above. Subsequently, we evaluated the retrieval of
unseen mentions referring using this \acrshort{kb}. We refer to this procedure
of linking to an index created from scratch starting from the mentions as
\textit{Linking to Mentions}, while the default configuration whose index is
created from the entities titles and descriptions is referred as \textit{Linking
to Entities}.


Section \ref{sec:linking-from-mentions} describes the technical aspects of the
experimental setup and provides the evaluation results.

\subsection{NIL Prediction} \label{sec:method-nil-prediction}

According to the definition, the NIL prediction task aims to detect if a mention
refers to an entity that is not present in the \acrshort{kb}. However,
considering the candidate ranking from the linker, ideally, in case the correct
entity for the mention is in the \acrshort{kb} it should correspond to the best
candidate for linking. Therefore the task of NIL prediction can be specified as
detecting whether the top-ranked entity for a mention is the correct one,
otherwise the mention is NIL. This approach does not expect the NIL prediction
module to correct the linker errors. Anyway, it could still mitigate linking
errors by answering NIL: in case the correct entity for a given mention exists
in the \acrshort{kb}, but the linker did not recognize it as the best candidate,
the NIL prediction module, in the ideal case, would recognize the best candidate
is not the correct one hence giving a NIL answer instead of linking to a wrong
entity. We consider this result worthwhile, especially in a context featuring a
\acrlong{hitl} process since, in our opinion, linking to an erroneous entity
would be harder to identify rather than wrongly linking to NIL.

Furthermore, instead of simply receiving an exact answer, such as
\textit{``correct''} or \textit{``not-correct''}, it is important to understand
how confident the system is when providing an answer \cite{krstajic2017binary}.
This is usually addressed by estimating a probability $p$: a real number within
the interval $[0,1]$ where, in this context, $p$ close to $0$ indicates the
mention is probably NIL and does not refer to the top-ranked entity, while $p$
close to $1$ means the top-ranked entity likely is the correct one. Instead, a
neutral $p = 0.5$ could be translated with answers like \textit{``don't know''}
or \textit{``50:50''}.
%
In a context that includes a \acrlong{hitl} process, a system able to estimate
the uncertainty would allow limiting the human intervention to only validate
uncertain decisions. As an example, answers like \textit{``the entity is correct
with an estimated probability of 0.83''} or \textit{``the entity is correct with
an estimated probability of 0.21''} could be considered acceptable while for
others like \textit{``the entity is correct with an estimated probability of
0.51''} a human validation could be required.
%
Finally, by establishing when a decision is too uncertain that it needs human
validation, using thresholds for instance, it would be possible to balance
performance and human effort according to the context demands. Indeed, this is
the answer we propose to the research question \ref{rq-hitl} (\textit{How to
best integrate the system with a \acrlong{hitl} process?}). The ability of the
NIL prediction module to estimate the uncertainty has been evaluated and the
results are available in section \ref{sec:hitl}.

\subsubsection{Formal Task Definition}

Given $M$ the set of all the possible mentions, $E$ the set of the entities, and
$C \subseteq M \times E$ the set of all the correct linking pairs $(m,e) \in M
    \times E$ such that $e$ is the entity to which $m$ refers, we first define the
NIL prediction as a binary classification corresponding to the function
$f_{nil}^b: M \times E \rightarrow \{0,1\}$, where:
\begin{equation}
    f_{nil}^b(m,e)=
    \begin{cases}
        1, & \text{if}\; (m,e) \in C \\
        0, & \text{otherwise}
    \end{cases}
\end{equation}
In other words, $f_{nil}^b(m,e)$ is 1 when $m$ refers to $e$ and 0 otherwise.

Given $l_{top}: M \rightarrow E$ the function giving the best candidate entity
for a mention according to the linker, in a real case it is reasonable to
consider $e_m^{top} = l_{top}(m)$, the best candidate entity for $m$, and
calculate $y_{top}^b = f_{nil}^b(m,e_m^{top})$, where $y_{top}^b = 1$ means that
the linking is correct, while $y_{top}^b = 0$ that $m$ is NIL since $e_m^{top}$,
the best candidate entity for $m$, is not the correct one.

On top of $f_{nil}^b$, with the aim of estimate the confidence of the
prediction, we define $f_{nil}^p: M \times E \rightarrow [0,1]$ as follows:
\begin{equation}
    f_{nil}^p(m,e) = P(f_{nil}^b(m,e) = 1))
\end{equation}
given $P(k) \in [0,1]$, the probability of the $k$ to be true.

In a real case considering $e_m^{top}$ we obtain:
\begin{equation}
    y_{top}^p = f_{nil}^p(m,e_m^{top})
\end{equation}
where $y_{top}^p$ close to $1$ means that the linking is probably correct, while
$y_{top}^p$ close to $0$ that $m$ is probably NIL. Instead, a value of
$y_{top}^p$ close to $0.5$ signifies that the prediction is uncertain and that
human validation may be worthwhile.



% Let $m$ be the considered mention to link, $e$ the best candidate according to
% the \acrshort{nel} system, and $e_{gold}$ the correct entity to which $m$
% refers, we denote with $f_{nil}^b(m,e)$ the output of the NIL prediction, which
% consists in a binary classification, given $m$ and $e$:
% \begin{equation}
%     f_{nil}^b(m,e)=
%     \begin{cases}
%         1, & \text{if}\; e = e_{gold} \\
%         0, & \text{otherwise}
%     \end{cases}
% \end{equation}






\FloatBarrier
\subsubsection{Logistic Regression}
Since a sigmoid curve, such as the logistic function, is a natural choice for
modeling probabilities \cite{demaris1995tutorial}, we opted for a
\textit{Logistic Regression} model to design the NIL prediction classifier. Some
additional tests were done with a Random Forest and a simple 2-layered Neural
Network with ReLU activation functions and a sigmoid at the end, to understand
if they could exploit non-linear relations, but they both underperformed the
Logistic Regression in terms of performance and, especially the NN, in terms of
training time.

The \textit{Logistic Regression} model is based on the \textit{logistic
function} which is shown in equation \ref{eq:logistic-fun} and figure
\ref{fig:logistic-function}.

\begin{equation} \label{eq:logistic-fun}
    f(z) = \frac{1}{1 + e^{-z}}
\end{equation}
%
The logistic function domain is within $[-\infty,+\infty]$ while its range is
bounded within $[0,1]$, making it suitable for describing probabilities. In a
problem involving multiple variables, we talk about \textit{Logistic model}; we
write $z = c + \sum w_{i}X_{i}$ where $X_i$ represent the variables' values
while $w_{i}$ and $c$ are constant terms representing unknown parameters.
Considering a variable $E$ where $E=0$ could mean, in our case, \textit{``the
mention is NIL''} we suppose we are interested in the probability of a mention
being not-NIL ($E=1$) given some known variables $X_{i}$. Then, the
\textit{Logistic model} is defined as:.
\begin{equation} \label{eq:logistic-model}
    P(E=1|X_{1},X_{2},\dots{}X_{k}) = \frac{1}{1 + e^{-(c + \sum w_{i}X_{i})}}
\end{equation}
Supposing the values of the parameters $c$ and $w_{i}$ are known it would be
possible to calculate $P(E=1)$ given the variables $X_{i}$
\cite{kleinbaum2002logistic}. Therefore the training of the Logistic Regression
model consist in estimating those parameters.

\begin{figure}[h!]
    \centering
    \begin{tikzpicture}
        \begin{axis}%
            [
                % grid=major,
                xmin=-6,
                xmax=6,
                axis x line=bottom,
                ytick={0,.5,1},
                ymax=1,
                axis y line=middle,
                xticklabels={$-\infty$,$-\infty$,,,,,,$+\infty$},
                % ticks=none,
                xlabel={z},
                ylabel={f(z)}
            ]
            \addplot%
            [
                black,%
                mark=none,
                samples=100,
                domain=-10:10,
            ]
            (x,{1/(1+exp(-x))});
        \end{axis}
    \end{tikzpicture}
    \label{fig:logistic-function}
    \caption{The Logistic function. See equation \ref{eq:logistic-fun}}
\end{figure}



\FloatBarrier
\subsubsection{Features}
% che features ho considerato?
% analisi delle features in results/experiments

Firstly, we considered essential to include the BLINK linking scores of the best
candidate entity as features for the NIL prediction; both the scores from the
bi-encoder and from the cross-encoder have been included.


% \paragraph{Scores statistics}
Then, similarly to \citet{zhou2013description}, some simple statistics of the
top-k candidates scores have been calculated and included as features to provide
the classifier with information about all the top-ranked candidates' scores.
These are the mean, the median, and the standard deviation, while the maximum
corresponds to the score of the best candidate that is already included as a feature.
%
Section \ref{sec:feature-selection} provides a preliminary analysis of these
statistical features.

% \paragraph{Mention-title text similarity}
Furthermore, we included the text similarity between the mention surface form
and the title of the best candidate for linking among the features. Several
different similarities have been considered. A preliminary analysis is available
in section \ref{sec:feature-selection}.

% \paragraph{NER types}
Since recent works demonstrated fine-grained entity typing improves linking
\cite{raiman2018deeptype,onoe2020finegrained}, the available typing information
for both the mention and the best candidate entity have been included in the
features. For the mentions, we considered the coarse-grained \acrshort{ner}
types (PER, LOC, ORG, MISC) that are available\footnote{AIDA normally does not
    contain NER types, but the provided jar was modified to keep types from
    CoNLL2013.} for the AIDA dataset. For the entities, instead, generally
\acrshort{kb}s contain typing information, but the BLINK
\acrlong{kb} only refers to Wikipedia resources, so the candidate entity types
have been extracted
% \begin{enumerate}
%     \item the ``average type'' of an entity $e$ corresponds to the relative
%     frequence or the empiric probability of $e$ to be referred by a mention of
%     type $t$: if all the mentions of $e$ are of kind PER it would be
%     ``PER=1, LOC=0, ORG=0, MISC=0'', whereas if half of
%     the mentions are ORG and half LOC: "PER=0, LOC=0.5,
%     ORG=0.5, MISC=0".
%     \item obtaining the ``average type'' the same way but filtering out BLINK
%     mistakes, considering only correct mention-entity pairs. Unfortunately this
%     drastically reduces the size of the dataset: the 34929 instances from the
%     AIDA datasets, of which 11858 NIL and 23071 not NIL, are reduced to only
%     26876, of which only 3805 NIL.
from Wikidata\footnote{https://www.wikidata.org/}, whose entities correspond to
Wikipedia resources. Wikidata types are way more specific, but basic NER types
(PER, LOC, and ORG) have been identified following Wikidata subclass property.
The MISC type has been assigned to those entities not represented by none of the
basic types. Note that entities can be of multiple types at the same time: for
example, Germany is both LOC and ORG since it is an instance of both ``country''
and ``state'' which respectively are subclasses of ``location'' and
``organization''. Both mention's and entity's types have been converted in a
one-hot encoding.


% \end{enumerate}

The considered features can be summarized as follows:
\begin{enumerate}
    \item the linking scores of the best candidate entity from both bi-encoder
          and cross-encoder (it corresponds to the maximum score of the top-k);
    \item the statistics about the scores of the top-k candidates for linking,
    consider both encoders' scores separately:
          \begin{enumerate}
              \item the mean,
              \item the median,
              \item the standard deviation.
          \end{enumerate}
    \item the text similarity between the mention surface form and the title of
          the best candidate entity;
          % todo! is it clear what the surface form is?
    \item the NER type of the mention (one-hot encoded);
    \item the type of the best candidate entity (one-hot encoded).
\end{enumerate}

In order to understand the impact of the different features on the NIL
prediction, an ablation study has been performed; the experimental details as
well as the results of the study are available in the section
\ref{sec:features-ablation-study}.

Finally, we conducted another ablation study of the NIL prediction features, but
with the approach \textit{Linking to Mentions}. The experiment aims to evaluate
the designed module in a \acrshort{kbp} context and to understand which NIL
prediction features generalize to a different \acrshort{kb} created from the
mentions. Indeed, for answering the research question \ref{rq-kb-independent}
(\textit{How can we keep the system independent from the \acrshort{kb}?}), the
NIL prediction module cannot rely on \acrshort{kb}-dependent features. Results
are shown in section \ref{sec:linking-from-mentions-nil}.

